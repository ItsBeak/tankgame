﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Raylib;
using MathLibrary;

namespace Project2D
{
    class GameObject
    {
        public Vector2 position;
        public Vector2 pivotPoint;
       
        public float rotation;

        public Matrix4 localTransform;
        public MathLibrary.Vector4 velocity;

        public Texture2D texture;
        private Texture2D image;

        //Holds all data for an object
        public GameObject(float xPos, float yPos, float rotation, Texture2D image)
        {
            position = new Vector2(xPos, yPos);
            this.rotation = rotation;
            this.image = image;
            texture = image;
            localTransform = new Matrix4();
            localTransform.CreateTranslation(position.x, position.y, 0);
        }

        //Runs every frame, and updates the position of the object
        public void Update()
        {
            Matrix4 translationMatrix = new Matrix4();
            translationMatrix.CreateTranslation(position.x, position.y, 0);

            Matrix4 rotationMatrix = Game.CreateRotationAboutPoint(pivotPoint.x, pivotPoint.y, rotation);

            localTransform = translationMatrix * rotationMatrix;
        }
    }
}
