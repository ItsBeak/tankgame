﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Raylib;
using static Raylib.Raylib;
using MathLibrary;

namespace Project2D
{
    class Game
    {
        Stopwatch stopwatch = new Stopwatch();
      
        private long currentTime = 0;
        private long lastTime = 0;
        private float timer = 0;
        private int fps = 1;
        private int frames;

        private float deltaTime = 0.005f;

        Image tankSprite;
        Texture2D tankTexture;

        Image turretSprite;
        Texture2D turretTexture;

        Image bulletSprite;
        Texture2D bulletTexture;

        GameObject playerTank;
        public float tankSpeed = 100f;

        GameObject playerTurret;

        public List<GameObject> bullets; 


        public Game()
        {

        }

        public void Init()
        {
            stopwatch.Start();
            lastTime = stopwatch.ElapsedMilliseconds;

            if (Stopwatch.IsHighResolution)
            {
                Console.WriteLine("Stopwatch high-resolution frequency: {0} ticks per second", Stopwatch.Frequency);
            }

            //Loads in the tank sprite and texture
            tankSprite = LoadImage("../Images/tankBody.png");
            tankTexture = LoadTextureFromImage(tankSprite);
            
            //Loads in the turret sprite and texture
            turretSprite = LoadImage("../Images/tankBarrel.png");
            turretTexture = LoadTextureFromImage(turretSprite);

            //Loads in the bullet sprite and texture
            bulletSprite = LoadImage("../Images/bullet.png");
            bulletTexture = LoadTextureFromImage(bulletSprite);

            //Instantiates the tank body and turret
            playerTank = new GameObject(GetScreenWidth() / 2, GetScreenHeight() / 2, 0, tankTexture);
            playerTurret = new GameObject(30, 30, 0, turretTexture);

            //Sets the pivot points for the textures
            playerTank.pivotPoint.x = 41;
            playerTank.pivotPoint.y = 40;

            playerTurret.pivotPoint.x = 12;
            playerTurret.pivotPoint.y = 4;

            //Creates the list that holds all the bullets
            bullets = new List<GameObject>();


        }

        public void Shutdown()
        {

        }

        public void Update()
        {
            lastTime = currentTime;
            currentTime = stopwatch.ElapsedMilliseconds;
            deltaTime = (currentTime - lastTime) / 1000.0f;
            timer += deltaTime;
            if (timer >= 1)
            {
                fps = frames;
                frames = 0;
                timer -= 1;
            }
            frames++;

            // insert game logic here     

            //The velocity variable for moving the tank
            MathLibrary.Vector4 tankVelocity = new MathLibrary.Vector4(0, 100, 0, 0);
            tankVelocity = playerTank.localTransform * tankVelocity;

            //Takes input from the keyboard to move the tank and turret
            if (IsKeyDown(KeyboardKey.KEY_W) == true)
            {
                playerTank.position.x += tankVelocity.x * deltaTime;
                playerTank.position.y += tankVelocity.y * deltaTime;
            }

            if (IsKeyDown(KeyboardKey.KEY_S) == true)
            {
                playerTank.position.x -= tankVelocity.x * deltaTime;
                playerTank.position.y -= tankVelocity.y * deltaTime;
            }

            if (IsKeyDown(KeyboardKey.KEY_D) == true)
            {
                playerTank.rotation += 1 * deltaTime;
            }

            if (IsKeyDown(KeyboardKey.KEY_A) == true)
            {
                playerTank.rotation -= 1 * deltaTime;
            }

            if (IsKeyDown(KeyboardKey.KEY_Q) == true)
            {
                playerTurret.rotation -= 1 * deltaTime;
            }

            if (IsKeyDown(KeyboardKey.KEY_E) == true)
            {
                playerTurret.rotation += 1 * deltaTime;
            }

            //Takes input from the keyboard to fire a bullet
            if (IsKeyPressed(KeyboardKey.KEY_SPACE) == true)
            {
                CreateBullet(playerTank.rotation, playerTurret.rotation);
            }

            //Runs the update function on both the body and the turret
            playerTank.Update();
            playerTurret.Update();

            //Prints the current bullet count to console
            Console.WriteLine("Bullet count: " + bullets.Count);

            //Checks if a bullet has left the bounds of the screen, if so, it is deleted
            for (int i = 0; i < bullets.Count; i++)
            {
                if (bullets[i].position.x >= GetScreenWidth() + 20)
                {
                    bullets.Remove(bullets[i]);
                }
                else if (bullets[i].position.x <= -20)
                {
                    bullets.Remove(bullets[i]);
                }
                else if (bullets[i].position.y >= GetScreenHeight() + 20)
                {
                    bullets.Remove(bullets[i]);
                }
                else if (bullets[i].position.y <= -20)
                {
                    bullets.Remove(bullets[i]);
                }
            }
        }

        public void Draw()
        {
            BeginDrawing();

            ClearBackground(Color.WHITE);

            DrawText(fps.ToString(), 10, 10, 14, Color.RED);

            DrawTextureTransformed(playerTank.texture, playerTank.localTransform);

            DrawTextureTransformed(playerTurret.texture, playerTank.localTransform * playerTurret.localTransform);

            //Updates the position of each bullet, and then draws it in its new position
            foreach (GameObject bullet in bullets)
            {
                bullet.position.x += bullet.velocity.x * deltaTime;
                bullet.position.y += bullet.velocity.y * deltaTime;

                bullet.pivotPoint.x = 7;
                bullet.pivotPoint.y = -30;

                bullet.Update();

                DrawTextureTransformed(bullet.texture, bullet.localTransform);
            }

            EndDrawing();
        }

        //Spawns a bullet at the tip of the turret
        public void CreateBullet(float tankRot, float turretRot)
        {
            GameObject bullet = new GameObject(playerTank.position.x + playerTurret.position.x + 5, playerTank.position.y + playerTurret.position.y + 30, 0, bulletTexture);

            bullets.Add(bullet);

            MathLibrary.Vector4 bulletVelocity = new MathLibrary.Vector4(0, 400, 0, 0);

            bullet.localTransform = playerTank.localTransform * playerTurret.localTransform;

            bulletVelocity = bullet.localTransform * bulletVelocity;

            bullet.velocity = bulletVelocity;

            bullet.rotation = tankRot + turretRot;
        }

        /// <summary>
        /// This function acts as a middleman of sorts, to translate your transform matrix
        /// into parameters for Raylib's built in DrawTexture function.
        /// It will pull the rotation, the scale, and the translation out of the matrix you
        /// pass in, and use that to draw the sprite in question.
        /// Due to (as far as I can tell) limitations on Raylib, this won't behave correctly
        /// for all possible matrices - it won't handle shear, for instance - but it should
        /// be okay for the basic stuff you can create with your matrix class.
        /// </summary>
        /// <param name="tex">The texture to draw.</param>
        /// <param name="transform">The transform to apply to the texture</param>
        static void DrawTextureTransformed(Texture2D tex, MathLibrary.Matrix4 transform)
        {

            Vector2 xAxis = new Vector2(transform.m1, transform.m2);
            Vector2 yAxis = new Vector2(transform.m5, transform.m6);
            Vector2 translation = new Vector2(transform.m13, transform.m14);
            //Note - the angle of a unit circle count up when they go anticlockwise,
            //but because in Raylib land positive Y is down, a positive angle will
            //be clockwise.
            float angle = (float)Math.Atan2(xAxis.y, xAxis.x);
            float scale = (xAxis.Length() + yAxis.Length()) / 2.0f;

            //We multiply the angle by 180/pi because the matrix library for the project
            //works in radians (that's what the unit tests require) but RayLib expects degrees.
            DrawTextureEx(tex, translation, angle * 180.0f / (float)Math.PI, scale, Color.WHITE);

        }
        public static Matrix4 CreateRotationAboutPoint(float x, float y, float angle)
        {
            Matrix4 rotation = new Matrix4();
            Matrix4 translationToOrigin = new Matrix4();
            Matrix4 translationFromOrigin = new Matrix4();

            translationFromOrigin.CreateTranslation(x, y, 0);
            translationToOrigin.CreateTranslation(-x, -y, 0);
            rotation.SetRotateZ(angle);

            return translationFromOrigin * rotation * translationToOrigin;
        }

    }
}
