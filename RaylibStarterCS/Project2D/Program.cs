﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Raylib;
using static Raylib.Raylib;

namespace Project2D
{
    class Program
    {
        static void Main(string[] args)
        {
            Game game = new Game();

            InitWindow(1280, 720, "Tank Game");

            SetTargetFPS(60);

            game.Init();

            while (!WindowShouldClose())
            {
                game.Update();
                game.Draw();
            }           

            game.Shutdown();

            CloseWindow();
        }
    }
}
