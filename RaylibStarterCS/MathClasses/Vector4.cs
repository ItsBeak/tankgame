﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MathLibrary
{
    public class Vector4
    {
        public float x, y, z, w;

        public Vector4()
        {
            x = 0;
            y = 0;
            z = 0;
            w = 0;
        }

        public Vector4(float xInit, float yInit, float zInit, float wInit)
        {
            x = xInit;
            y = yInit;
            z = zInit;
            w = wInit;
        }

        public static Vector4 operator +(Vector4 vecA, Vector4 vecB)
        {
            Vector4 vec3 = new Vector4();
            vec3.x = vecA.x + vecB.x;
            vec3.y = vecA.y + vecB.y;
            vec3.z = vecA.z + vecB.z;
            vec3.w = vecA.w + vecB.w;

            return vec3;
        }

        public static Vector4 operator -(Vector4 vecA, Vector4 vecB)
        {
            Vector4 vec3 = new Vector4();
            vec3.x = vecA.x - vecB.x;
            vec3.y = vecA.y - vecB.y;
            vec3.z = vecA.z - vecB.z;
            vec3.w = vecA.w - vecB.w;

            return vec3;
        }
        public static Vector4 operator *(Vector4 vec1, float val)
        {
            Vector4 vec3 = new Vector4();
            vec3.x = vec1.x * val;
            vec3.y = vec1.y * val;
            vec3.z = vec1.z * val;
            vec3.w = vec1.w * val;

            return vec3;
        }
        public static Vector4 operator *(float val, Vector4 vec1)
        {
            Vector4 vec3 = new Vector4();
            vec3.x = vec1.x * val;
            vec3.y = vec1.y * val;
            vec3.z = vec1.z * val;
            vec3.w = vec1.w * val;

            return vec3;
        }
        public float Dot(Vector4 vec)
        {
            float dot = this.x * vec.x + this.y * vec.y + this.z * vec.z;

            return dot;
        }
        public Vector4 Cross(Vector4 vec)
        {
            return new Vector4(
                this.y * vec.z - this.z * vec.y,
                this.z * vec.x - this.x * vec.z,
                this.x * vec.y - this.y * vec.x,
                this.w * vec.w);
        }
        public float Magnitude()
        {
            float mag;
            mag = (x * x + y * y + z * z);

            return (float)Math.Sqrt(mag);
        }
        public Vector4 Normalize()
        {
            float mag = this.Magnitude();
            this.x /= mag;
            this.y /= mag;
            this.z /= mag;

            return this;
        }
        public static Vector4 operator *(Vector4 vec, Matrix4 mat)
        {
            Vector4 finalVec = new Vector4();

            finalVec.x = (vec.x * mat.m1 + vec.y * mat.m5 + vec.z * mat.m9 + vec.w * mat.m13);
            finalVec.y = (vec.x * mat.m2 + vec.y * mat.m6 + vec.z * mat.m10 + vec.w * mat.m14);
            finalVec.z = (vec.x * mat.m3 + vec.y * mat.m7 + vec.z * mat.m11 + vec.w * mat.m15);
            finalVec.w = (vec.x * mat.m4 + vec.y * mat.m8 + vec.z * mat.m12 + vec.w * mat.m16);

            return finalVec;
        }
        public static Vector4 operator *(Matrix4 mat, Vector4 vec)
        {
            Vector4 finalVec = new Vector4();

            finalVec.x = (vec.x * mat.m1 + vec.y * mat.m5 + vec.z * mat.m9 + vec.w * mat.m13);
            finalVec.y = (vec.x * mat.m2 + vec.y * mat.m6 + vec.z * mat.m10 + vec.w * mat.m14);
            finalVec.z = (vec.x * mat.m3 + vec.y * mat.m7 + vec.z * mat.m11 + vec.w * mat.m15);
            finalVec.w = (vec.x * mat.m4 + vec.y * mat.m8 + vec.z * mat.m12 + vec.w * mat.m16);

            return finalVec;
        }
    }
}
