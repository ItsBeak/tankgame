﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MathLibrary
{
    public class Vector3
    {
        public float x, y, z;

        public Vector3()
        {
            x = 0;
            y = 0;
            z = 0;
        }

        public Vector3(float xInit, float yInit, float zInit)
        {
            x = xInit;
            y = yInit;
            z = zInit;
        }

        public static Vector3 operator+ (Vector3 vec1, Vector3 vec2)
        {
            Vector3 vec3 = new Vector3();
            vec3.x = vec1.x + vec2.x;
            vec3.y = vec1.y + vec2.y;
            vec3.z = vec1.z + vec2.z;

            return vec3;
        }

        public static Vector3 operator -(Vector3 vec1, Vector3 vec2)
        {
            Vector3 vec3 = new Vector3();
            vec3.x = vec1.x - vec2.x;
            vec3.y = vec1.y - vec2.y;
            vec3.z = vec1.z - vec2.z;

            return vec3;
        }
        public static Vector3 operator *(Vector3 vec1, float val)
        {
            Vector3 vec3 = new Vector3();
            vec3.x = vec1.x * val;
            vec3.y = vec1.y * val;
            vec3.z = vec1.z * val;

            return vec3;
        }

        public static Vector3 operator *(float val, Vector3 vec1)
        {
            Vector3 vec3 = new Vector3();
            vec3.x = vec1.x * val;
            vec3.y = vec1.y * val;
            vec3.z = vec1.z * val;

            return vec3;
        }

        public float Dot(Vector3 vec)
        {
            float dot = this.x * vec.x + this.y * vec.y + this.z * vec.z;
           
            return dot;           
        }

        public Vector3 Cross(Vector3 vec)
        {
            return new Vector3(
                this.y * vec.z - this.z * vec.y,
                this.z * vec.x - this.x * vec.z,
                this.x * vec.y - this.y * vec.x);    
        }

        public float Magnitude()
        {
            float mag;

            mag = (x * x + y * y + z * z);

            return (float)Math.Sqrt(mag);
        }

        public Vector3 Normalize()
        {
            float mag = this.Magnitude();
            this.x /= mag;
            this.y /= mag;
            this.z /= mag;

            return this;
        }

        public static Vector3 operator *(Vector3 vec, Matrix3 mat)
        {
            Vector3 finalVec = new Vector3();

            finalVec.x = (vec.x * mat.m1 + vec.y * mat.m4 + vec.z * mat.m7);
            finalVec.y = (vec.x * mat.m2 + vec.y * mat.m5 + vec.z * mat.m8);
            finalVec.z = (vec.x * mat.m3 + vec.y * mat.m6 + vec.z * mat.m9);

            return finalVec;
        }

        public static Vector3 operator *(Matrix3 mat, Vector3 vec)
        {
            Vector3 finalVec = new Vector3();

            finalVec.x = (vec.x * mat.m1 + vec.y * mat.m4 + vec.z * mat.m7);
            finalVec.y = (vec.x * mat.m2 + vec.y * mat.m5 + vec.z * mat.m8);
            finalVec.z = (vec.x * mat.m3 + vec.y * mat.m6 + vec.z * mat.m9);

            return finalVec;
        }

    }
}
