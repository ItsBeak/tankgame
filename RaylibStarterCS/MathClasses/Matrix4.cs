﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MathLibrary
{
    public class Matrix4
    {
        public float m1, m2, m3, m4, m5, m6, m7, m8, m9, m10, m11, m12, m13, m14, m15, m16;

        public Matrix4()
        {
            m1 = 1; m2 = 0; m3 = 0; m4 = 0;
            m5 = 0; m6 = 1; m7 = 0; m8 = 0;
            m9 = 0; m10= 0; m11= 1; m12= 0;
            m13= 0; m14= 0; m15= 0; m16= 1;
        }
        public Matrix4(float a1, float a2, float a3, float a4, float a5, float a6, float a7, float a8, float a9, float a10, float a11, float a12, float a13, float a14, float a15, float a16)
        {
            m1 = a1; m2 = a2; m3 = a3; m4 = a4;
            m5 = a5; m6 = a6; m7 = a7; m8 = a8;
            m9 = a9; m10 = a10; m11 = a11; m12 = a12;
            m13 = a13; m14 = a14; m15 = a15; m16 = a16;
        }

        public void SetRotateX(double radians)
        {
            this.m1 = 1; this.m2 = 0; this.m3 = 0; this.m4 = 0;
            this.m5 = 0; this.m6 = (float)Math.Cos(radians); this.m7 = (float)Math.Sin(radians); this.m8 = 0;
            this.m9 = 0; this.m10 = (float)-Math.Sin(radians); this.m11 = (float)Math.Cos(radians); this.m12 = 0;
            this.m13 = 0; this.m14 = 0; this.m15 = 0; this.m16 = 1;
        }
        public void SetRotateY(double radians)
        {
            this.m1 = (float)Math.Cos(radians); this.m2 = 0; this.m3 = (float)-Math.Sin(radians); this.m4 = 0;
            this.m5 = 0; this.m6 = 1; this.m7 = 0; this.m8 = 0;
            this.m9 = (float)Math.Sin(radians); this.m10 = 0; this.m11 = (float)Math.Cos(radians); this.m12 = 0;
            this.m13 = 0; this.m14 = 0; this.m15 = 0; this.m16 = 1;
        }
        public void SetRotateZ(double radians)
        {
            this.m1 = (float)Math.Cos(radians); this.m2 = (float)Math.Sin(radians); this.m3 = 0; this.m4 = 0;
            this.m5 = (float)-Math.Sin(radians); this.m6 = (float)Math.Cos(radians); this.m7 = 0; this.m8 = 0;
            this.m9 = 0; this.m10 = 0; this.m11 = 1; this.m12 = 0;
            this.m13 = 0; this.m14 = 0; this.m15 = 0; this.m16 = 1;
        }

        public void CreateTranslation(float x, float y, float z)
        {
            this.m13 = x;
            this.m14 = y;
            this.m15 = z;
        }

        public static Matrix4 operator *(Matrix4 mat2, Matrix4 mat1)
        {
            Matrix4 newMat = new Matrix4();

            newMat.m1 = mat1.m1 * mat2.m1 + mat1.m2 * mat2.m5 + mat1.m3 * mat2.m9 + mat1.m4 * mat2.m13;
            newMat.m2 = mat1.m1 * mat2.m2 + mat1.m2 * mat2.m6 + mat1.m3 * mat2.m10 + mat1.m4 * mat2.m14;
            newMat.m3 = mat1.m1 * mat2.m3 + mat1.m2 * mat2.m7 + mat1.m3 * mat2.m11 + mat1.m4 * mat2.m15;
            newMat.m4 = mat1.m1 * mat2.m4 + mat1.m2 * mat2.m8 + mat1.m3 * mat2.m12 + mat1.m4 * mat2.m16;

            newMat.m5 = mat1.m5 * mat2.m1 + mat1.m6 * mat2.m5 + mat1.m7 * mat2.m9 + mat1.m7 * mat2.m13;
            newMat.m6 = mat1.m5 * mat2.m2 + mat1.m6 * mat2.m6 + mat1.m7 * mat2.m10 + mat1.m7 * mat2.m14;
            newMat.m7 = mat1.m5 * mat2.m3 + mat1.m6 * mat2.m7 + mat1.m7 * mat2.m11 + mat1.m7 * mat2.m15;
            newMat.m8 = mat1.m5 * mat2.m4 + mat1.m6 * mat2.m7 + mat1.m7 * mat2.m12 + mat1.m7 * mat2.m16;

            newMat.m9 = mat1.m9 * mat2.m1 + mat1.m10 * mat2.m5 + mat1.m11 * mat2.m9 + mat1.m12 * mat2.m13;
            newMat.m10 = mat1.m9 * mat2.m2 + mat1.m10 * mat2.m6 + mat1.m11 * mat2.m10 + mat1.m12 * mat2.m14;
            newMat.m11 = mat1.m9 * mat2.m3 + mat1.m10 * mat2.m7 + mat1.m11 * mat2.m11 + mat1.m12 * mat2.m15;
            newMat.m12 = mat1.m9 * mat2.m4 + mat1.m10 * mat2.m8 + mat1.m11 * mat2.m12 + mat1.m12 * mat2.m16;

            newMat.m13 = mat1.m13 * mat2.m1 + mat1.m14 * mat2.m5 + mat1.m15 * mat2.m9 + mat1.m16 * mat2.m13;
            newMat.m14 = mat1.m13 * mat2.m2 + mat1.m14 * mat2.m6 + mat1.m15 * mat2.m10 + mat1.m16 * mat2.m14;
            newMat.m15 = mat1.m13 * mat2.m3 + mat1.m14 * mat2.m7 + mat1.m15 * mat2.m11 + mat1.m16 * mat2.m15;
            newMat.m16 = mat1.m13 * mat2.m4 + mat1.m14 * mat2.m8 + mat1.m15 * mat2.m12 + mat1.m16 * mat2.m16;

            return newMat;
        }
    }
}
